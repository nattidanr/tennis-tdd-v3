// State
/*
- PlayerA's score
- PlayerB's score
- Match result
*/

// Behavior
/*
- PlayerA get score
- PlayerB get score
- Reset score
- PlayerA wins the game
- PlayerB wins the game
- Echo score
*/

function TennisGame() {
    this.playerAScore
    this.playerBScore

    function checkScore(score) {
        if (score === 0) {
            return 'Love'
        } else if (score === 15) {
            return 'Fifteen'
        } else if (score === 30) {
            return 'Thirty'
        } else if (score === 40) {
            return 'Forty'
        }
    }

    this.start = () => {
        this.playerAScore = 0
        this.playerBScore = 0
    }

    this.echoScore = () => {
        if (this.playerAScore === 40 && this.playerBScore === 40) {
            return 'Deuce'
        } else if (this.playerAScore >= 40 && this.playerBScore === 40) {
            return 'PlayerA advantage'
        } else if (this.playerBScore > 40) {
            return 'PlayerB wins game'
        } else if (this.playerAScore > 40) {
            return 'PlayerA wins game'
        } else {
            var playerAScoreString = checkScore(this.playerAScore)
            var playerBScoreString = checkScore(this.playerBScore)
            var scoreString = playerAScoreString + ' - ' + playerBScoreString
            return scoreString
        }
    }

    this.playerAGetScore = () => {
        if (this.playerAScore === 30) {
            this.playerAScore += 10
        } else {
            this.playerAScore += 15
        }
    }

    this.playerBGetScore = () => {
        if (this.playerBScore === 30) {
            this.playerBScore += 10
        } else {
            this.playerBScore += 15
        }
    }
}

var tennisGame

function startGame() {
    tennisGame = new TennisGame()
    tennisGame.start()
}

function addScorePlayerA(count) {
    for (let i = 0; i < count; i++) {
        tennisGame.playerAGetScore()
    }
}

function addScorePlayerB(count) {
    for (let i = 0; i < count; i++) {
        tennisGame.playerBGetScore()
    }
}

test('"Love - Love" when game started', () => {
    startGame()

    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Love - Love')
})

test('"Fifteen - Love" when A get first score', () => {
    startGame()
    addScorePlayerA(1)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Fifteen - Love')
})

test('"Thirty - Love" when A get double scored', () => {
    startGame()
    addScorePlayerA(2)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Thirty - Love')
})

test('"Forty - Love" when A get triple score', () => {
    startGame()
    addScorePlayerA(3)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Forty - Love')
})

test('"PlayerA wins game when A get 4 scores before B"', () => {
    startGame()

    addScorePlayerA(4)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerA wins game')
})

test('"Love - Fifteen" when B get first score', () => {
    startGame()
    addScorePlayerB(1)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Love - Fifteen')
})

test('"Love - Thirty" when B get double scored', () => {
    startGame()
    addScorePlayerB(2)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Love - Thirty')
})

test('"PlayerB wins game when B get 4 scores before A"', () => {
    startGame()

    addScorePlayerB(4)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerB wins game')
})

test('"PlayerA wins game when A get 4 scores before B"', () => {
    startGame()

    addScorePlayerA(4)
    addScorePlayerB(2)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerA wins game')
})

test('"Deuce" when score is 40-40', () => {
    startGame()
    addScorePlayerA(3)
    addScorePlayerB(3)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Deuce')
})

test('"PlayerA advantage" when A get score after deuce', () => {
    startGame()
    addScorePlayerA(3)
    addScorePlayerB(3)
    tennisGame.playerAGetScore()
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerA advantage')
})